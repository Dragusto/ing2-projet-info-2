#include "Damier.h"
#include "Pion.h"
#include <iostream>
#include <vector>
#include <fstream>
#include "console.h"
#include <windows.h>
#include <conio.h>
#include <fstream>

using namespace std;

Damier::Damier()
{
    //initialisation pion depart
    Pion p1('N',5,4);
    Pion p2('N',4,5);
    Pion p3('B',4,4);
    Pion p4('B',5,5);


    Pionjouer.clear();


   Pionjouer.push_back(p1);
    Pionjouer.push_back(p2);
    Pionjouer.push_back(p3);
    Pionjouer.push_back(p4);



}

Damier::~Damier()
{
    //dtor
}

void Damier::AfficherDamier() ///modifier le 8 par une vairable si on veux un tableau modulable
{


    int x=0;
    std::cout<<"     A   B   C   D   E   F   G   H"<<std::endl;
    std::cout<<"   +---+---+---+---+---+---+---+---+"<<std::endl;
    for(int j=0; j<8; j++)    //coordonn�e colonne
    {
        std::cout<<x+1<<"  ";
        for(int i=0; i<8; i++) //coordonn�e ligne
        {
            int paff=0;
            for(unsigned int p=0; p<Pionjouer.size(); p++)
            {

                if((Pionjouer[p].Getx()==i+1)&&(Pionjouer[p].Gety()==j+1))
                {
                    std::cout<<"|";
                    Pionjouer[p].AfficherPion();
                    paff=1;
                    p=Pionjouer.size();

                }
            }
            if(paff==0)
            {
                std::cout<<"|   ";
            }

        }
        std::cout<<"|  "<<x+1<<" ";
        std::cout<<std::endl;
        std::cout<<"   +---+---+---+---+---+---+---+---+  "<<std::endl;
        x=x+1;
    }
    std::cout<<"     A   B   C   D   E   F   G   H"<<std::endl;



}


void Damier::AjouterPion(char tour,int x, int y)
{
    int op;
    if(tour=='N')
    {
        op='B';
    }
    else if (tour=='B')
    {
        op='N';
    }
    Pion P(tour,x,y);
    //  Damier.PossibiJeu(couleur);      a blinder dans la selection du placement
    // if(TabPossibi[x-1][y-1]==1) //-1 car tab commence a 0 et x/y a 1

    Pionjouer.push_back(P);
    for(unsigned int p=0; p<Pionjouer.size(); p++)
    {

        ///vers la droite
        if((Pionjouer[p].Getx()==x+1)&&(Pionjouer[p].Gety()==y)&&(Pionjouer[p].Getcouleur()==op))
        {
            int dp=2;
            while((dp>0)&&(x+dp<=8))
            {
                int action=0;
                for(unsigned int p2=0; p2<Pionjouer.size(); p2++)
                {
                    if((Pionjouer[p2].Getx()==x+dp)&&(Pionjouer[p2].Gety()==y)&&(Pionjouer[p2].Getcouleur()==op)) //si le pion a droite est de la couleur du joeur oppos� on regarde encore droite
                    {

                        dp=dp+1;
                        action=1;

                    }
                    else if((Pionjouer[p2].Getx()==x+dp)&&(Pionjouer[p2].Gety()==y)&&(Pionjouer[p2].Getcouleur()==tour)) //si le pion est de la couleur du jouer on marque
                    {
                        while(dp>0)
                        {
                            for(unsigned int p2=0; p2<Pionjouer.size(); p2++)
                            {
                                if((Pionjouer[p2].Getx()==x+dp)&&(Pionjouer[p2].Gety()==y))
                                {
                                    Pionjouer[p2].Setcouleur(tour);

                                }
                            }
                            dp=dp-1;
                        }

                    }
                }
                if(action==0)
                {
                    dp=0;

                }



            }
        }

        ///vers la gauche
        if((Pionjouer[p].Getx()==x-1)&&(Pionjouer[p].Gety()==y)&&(Pionjouer[p].Getcouleur()==op))
        {
            int dp=2;
            while((dp>0)&&(x-dp>0))
            {
                int action=0;
                for(unsigned int p2=0; p2<Pionjouer.size(); p2++)
                {
                    if((Pionjouer[p2].Getx()==x-dp)&&(Pionjouer[p2].Gety()==y)&&(Pionjouer[p2].Getcouleur()==op)) //si le pion a droite est de la couleur du joeur oppos� on regarde encore droite
                    {

                        dp=dp+1;
                        action=1;

                    }
                    else if((Pionjouer[p2].Getx()==x-dp)&&(Pionjouer[p2].Gety()==y)&&(Pionjouer[p2].Getcouleur()==tour)) //si le pion est de la couleur du jouer on marque
                    {
                        while(dp>0)
                        {
                            for(unsigned int p2=0; p2<Pionjouer.size(); p2++)
                            {
                                if((Pionjouer[p2].Getx()==x-dp)&&(Pionjouer[p2].Gety()==y))
                                {
                                    Pionjouer[p2].Setcouleur(tour);

                                }
                            }
                            dp=dp-1;
                        }

                    }
                }
                if(action==0)
                {
                    dp=0;

                }



            }
        }

        ///vers le bas
        if((Pionjouer[p].Getx()==x)&&(Pionjouer[p].Gety()==y+1)&&(Pionjouer[p].Getcouleur()==op))
        {
            int dp=2;
            while((dp>0)&&(y+dp<=8))
            {
                int action=0;
                for(unsigned int p2=0; p2<Pionjouer.size(); p2++)
                {
                    if((Pionjouer[p2].Getx()==x)&&(Pionjouer[p2].Gety()==y+dp)&&(Pionjouer[p2].Getcouleur()==op)) //si le pion a droite est de la couleur du joeur oppos� on regarde encore droite
                    {

                        dp=dp+1;
                        action=1;

                    }
                    else if((Pionjouer[p2].Getx()==x)&&(Pionjouer[p2].Gety()==y+dp)&&(Pionjouer[p2].Getcouleur()==tour)) //si le pion est de la couleur du jouer on marque
                    {
                        while(dp>0)
                        {
                            for(unsigned int p2=0; p2<Pionjouer.size(); p2++)
                            {
                                if((Pionjouer[p2].Getx()==x)&&(Pionjouer[p2].Gety()==y+dp))
                                {
                                    Pionjouer[p2].Setcouleur(tour);

                                }
                            }
                            dp=dp-1;
                        }

                    }
                }
                if(action==0)
                {
                    dp=0;

                }



            }
        }
        ///vers le haut
        if((Pionjouer[p].Getx()==x)&&(Pionjouer[p].Gety()==y-1)&&(Pionjouer[p].Getcouleur()==op))
        {
            int dp=2;
            while((dp>0)&&(y-dp>0))
            {
                int action=0;
                for(unsigned int p2=0; p2<Pionjouer.size(); p2++)
                {
                    if((Pionjouer[p2].Getx()==x)&&(Pionjouer[p2].Gety()==y-dp)&&(Pionjouer[p2].Getcouleur()==op)) //si le pion a droite est de la couleur du joeur oppos� on regarde encore droite
                    {

                        dp=dp+1;
                        action=1;

                    }
                    else if((Pionjouer[p2].Getx()==x)&&(Pionjouer[p2].Gety()==y-dp)&&(Pionjouer[p2].Getcouleur()==tour)) //si le pion est de la couleur du jouer on marque
                    {
                        while(dp>0)
                        {
                            for(unsigned int p2=0; p2<Pionjouer.size(); p2++)
                            {
                                if((Pionjouer[p2].Getx()==x)&&(Pionjouer[p2].Gety()==y-dp))
                                {
                                    Pionjouer[p2].Setcouleur(tour);

                                }
                            }
                            dp=dp-1;
                        }

                    }
                }
                if(action==0)
                {
                    dp=0;

                }



            }
        }

        ///vers diagonale droite/bas
        if((Pionjouer[p].Getx()==x+1)&&(Pionjouer[p].Gety()==y+1)&&(Pionjouer[p].Getcouleur()==op))
        {
            int dp=2;
            while((dp>0)&&(x+dp<=8)&&(y+dp<=8))
            {
                int action=0;
                for(unsigned int p2=0; p2<Pionjouer.size(); p2++)
                {
                    if((Pionjouer[p2].Getx()==x+dp)&&(Pionjouer[p2].Gety()==y+dp)&&(Pionjouer[p2].Getcouleur()==op)) //si le pion a droite est de la couleur du joeur oppos� on regarde encore droite
                    {

                        dp=dp+1;
                        action=1;

                    }
                    else if((Pionjouer[p2].Getx()==x+dp)&&(Pionjouer[p2].Gety()==y+dp)&&(Pionjouer[p2].Getcouleur()==tour)) //si le pion est de la couleur du jouer on marque
                    {
                        while(dp>0)
                        {
                            for(unsigned int p2=0; p2<Pionjouer.size(); p2++)
                            {
                                if((Pionjouer[p2].Getx()==x+dp)&&(Pionjouer[p2].Gety()==y+dp))
                                {
                                    Pionjouer[p2].Setcouleur(tour);

                                }
                            }
                            dp=dp-1;
                        }

                    }
                }
                if(action==0)
                {
                    dp=0;

                }



            }
        }

        ///vers diagonale droite/haut
        if((Pionjouer[p].Getx()==x+1)&&(Pionjouer[p].Gety()==y-1)&&(Pionjouer[p].Getcouleur()==op))
        {
            int dp=2;
            while((dp>0)&&(x+dp<=8)&&(y-dp>0))
            {
                int action=0;
                for(unsigned int p2=0; p2<Pionjouer.size(); p2++)
                {
                    if((Pionjouer[p2].Getx()==x+dp)&&(Pionjouer[p2].Gety()==y-dp)&&(Pionjouer[p2].Getcouleur()==op)) //si le pion a droite est de la couleur du joeur oppos� on regarde encore droite
                    {

                        dp=dp+1;
                        action=1;

                    }
                    else if((Pionjouer[p2].Getx()==x+dp)&&(Pionjouer[p2].Gety()==y-dp)&&(Pionjouer[p2].Getcouleur()==tour)) //si le pion est de la couleur du jouer on marque
                    {
                        while(dp>0)
                        {
                            for(unsigned int p2=0; p2<Pionjouer.size(); p2++)
                            {
                                if((Pionjouer[p2].Getx()==x+dp)&&(Pionjouer[p2].Gety()==y-dp))
                                {
                                    Pionjouer[p2].Setcouleur(tour);

                                }
                            }
                            dp=dp-1;
                        }

                    }
                }
                if(action==0)
                {
                    dp=0;

                }



            }
        }

        ///vers diagonale gauche/bas
        if((Pionjouer[p].Getx()==x-1)&&(Pionjouer[p].Gety()==y+1)&&(Pionjouer[p].Getcouleur()==op))
        {
            int dp=2;
            while((dp>0)&&(x-dp>0)&&(y+dp<=8))
            {
                int action=0;
                for(unsigned int p2=0; p2<Pionjouer.size(); p2++)
                {
                    if((Pionjouer[p2].Getx()==x-dp)&&(Pionjouer[p2].Gety()==y+dp)&&(Pionjouer[p2].Getcouleur()==op)) //si le pion a droite est de la couleur du joeur oppos� on regarde encore droite
                    {

                        dp=dp+1;
                        action=1;

                    }
                    else if((Pionjouer[p2].Getx()==x-dp)&&(Pionjouer[p2].Gety()==y+dp)&&(Pionjouer[p2].Getcouleur()==tour)) //si le pion est de la couleur du jouer on marque
                    {
                        while(dp>0)
                        {
                            for(unsigned int p2=0; p2<Pionjouer.size(); p2++)
                            {
                                if((Pionjouer[p2].Getx()==x-dp)&&(Pionjouer[p2].Gety()==y+dp))
                                {
                                    Pionjouer[p2].Setcouleur(tour);

                                }
                            }
                            dp=dp-1;
                        }

                    }
                }
                if(action==0)
                {
                    dp=0;

                }



            }
        }


///vers diagonale gauche/haut
        if((Pionjouer[p].Getx()==x-1)&&(Pionjouer[p].Gety()==y-1)&&(Pionjouer[p].Getcouleur()==op))
        {
            int dp=2;
            while((dp>0)&&(x-dp>0)&&(y-dp>0))
            {
                int action=0;
                for(unsigned int p2=0; p2<Pionjouer.size(); p2++)
                {
                    if((Pionjouer[p2].Getx()==x-dp)&&(Pionjouer[p2].Gety()==y-dp)&&(Pionjouer[p2].Getcouleur()==op)) //si le pion a droite est de la couleur du joeur oppos� on regarde encore droite
                    {

                        dp=dp+1;
                        action=1;

                    }
                    else if((Pionjouer[p2].Getx()==x-dp)&&(Pionjouer[p2].Gety()==y-dp)&&(Pionjouer[p2].Getcouleur()==tour)) //si le pion est de la couleur du jouer on marque
                    {
                        while(dp>0)
                        {
                            for(unsigned int p2=0; p2<Pionjouer.size(); p2++)
                            {
                                if((Pionjouer[p2].Getx()==x-dp)&&(Pionjouer[p2].Gety()==y-dp))
                                {
                                    Pionjouer[p2].Setcouleur(tour);

                                }
                            }
                            dp=dp-1;
                        }

                    }
                }
                if(action==0)
                {
                    dp=0;

                }



            }
        }






    }



}

void Damier::PossibiJeu(char tour)
{
    char op;
    if(tour=='N')
    {
        op='B';
    }
    else if (tour=='B')
    {
        op='N';
    }
    int YaPion=0;
    for(int i=0; i<8; i++)    //initialisation de toute les case a 0
    {
        for(int j=0; j<8; j++)
        {
            TabPossibi[i][j]=0;
        }
    }



    for(int j=1; j<9; j++)
    {


        for(int i=1; i<9; i++)
        {
            YaPion=0;



            for(unsigned int p=0; p<Pionjouer.size(); p++)  //verification presence pion
            {
                if((Pionjouer[p].Getx()==i)&&(Pionjouer[p].Gety()==j)&&(Pionjouer[p].Getcouleur()==tour))
                {

                    YaPion=1;
                }
                else if((Pionjouer[p].Getx()==i)&&(Pionjouer[p].Gety()==j)&&(Pionjouer[p].Getcouleur()==op))
                {
                    YaPion=1;
                }
            }
            if (YaPion==0)
            {
                ///verification possibilit� vers la droite

                for(unsigned int p=0; p<Pionjouer.size(); p++)
                {

                    if((Pionjouer[p].Getx()==i+1)&&(Pionjouer[p].Gety()==j)&&(Pionjouer[p].Getcouleur()==op))
                    {
                        int dp=2;
                        while((dp>=2)&&(dp+i<=8))
                        {
                            int action=0;
                            for(unsigned int p2=0; p2<Pionjouer.size(); p2++)
                            {
                                if((Pionjouer[p2].Getx()==i+dp)&&(Pionjouer[p2].Gety()==j)&&(Pionjouer[p2].Getcouleur()==op)) //si le pion a droite est de la couleur du joeur oppos� on regarde encore droite
                                {

                                    dp=dp+1;
                                    action=1;

                                }
                                else if((Pionjouer[p2].Getx()==i+dp)&&(Pionjouer[p2].Gety()==j)&&(Pionjouer[p2].Getcouleur()==tour)) //si le pion est de la couleur du jouer on marque
                                {

                                    TabPossibi[i-1][j-1]=1;   //-1 car tableau de 0 a 7 et vecteur de 1 a 8
                                    dp=0;
                                    action=1;

                                }
                            }
                            if(action==0)
                            {

                                dp=0;

                            }



                        }
                    }

                }
                ///verification possibilit� vers la gauche
                for(unsigned int p=0; p<Pionjouer.size(); p++)
                {

                    if((Pionjouer[p].Getx()==i-1)&&(Pionjouer[p].Gety()==j)&&(Pionjouer[p].Getcouleur()==op))
                    {
                        int dp=2;
                        while((dp>=2)&&(i-dp>0))
                        {
                            int action=0;
                            for(unsigned int p2=0; p2<Pionjouer.size(); p2++)
                            {
                                if((Pionjouer[p2].Getx()==i-dp)&&(Pionjouer[p2].Gety()==j)&&(Pionjouer[p2].Getcouleur()==op)) //si le pion a droite est de la couleur du joeur oppos� on regarde encore droite
                                {

                                    dp=dp+1;
                                    action=1;

                                }
                                else if((Pionjouer[p2].Getx()==i-dp)&&(Pionjouer[p2].Gety()==j)&&(Pionjouer[p2].Getcouleur()==tour)) //si le pion est de la couleur du jouer on marque
                                {

                                    TabPossibi[i-1][j-1]=1;   //-1 car tableau de 0 a 7 et vecteur de 1 a 8
                                    dp=0;
                                    action=1;

                                }
                            }
                            if(action==0)
                            {
                                dp=0;

                            }



                        }
                    }

                }

                ///verification vers le bas

                for(unsigned int p=0; p<Pionjouer.size(); p++)
                {
                    if((Pionjouer[p].Getx()==i)&&(Pionjouer[p].Gety()==j+1)&&(Pionjouer[p].Getcouleur()==op))
                    {
                        int dp=2;
                        while((dp>=2)&&(dp+j<=8))
                        {
                            int action=0;
                            for(unsigned int p2=0; p2<Pionjouer.size(); p2++)
                            {
                                if((Pionjouer[p2].Getx()==i)&&(Pionjouer[p2].Gety()==j+dp)&&(Pionjouer[p2].Getcouleur()==op)) //si le pion a droite est de la couleur du joeur oppos� on regarde encore droite
                                {

                                    dp=dp+1;
                                    action=1;

                                }
                                else if((Pionjouer[p2].Getx()==i)&&(Pionjouer[p2].Gety()==j+dp)&&(Pionjouer[p2].Getcouleur()==tour)) //si le pion est de la couleur du jouer on marque
                                {

                                    TabPossibi[i-1][j-1]=1;   //-1 car tableau de 0 a 7 et vecteur de 1 a 8
                                    dp=0;
                                    action=1;

                                }
                            }
                            if(action==0)
                            {

                                dp=0;

                            }



                        }
                    }

                }
                ///verification vers le haut
                for(unsigned int p=0; p<Pionjouer.size(); p++)
                {
                    if((Pionjouer[p].Getx()==i)&&(Pionjouer[p].Gety()==j-1)&&(Pionjouer[p].Getcouleur()==tour))
                    {

                        YaPion=1;  //futile
                    }
                    else if((Pionjouer[p].Getx()==i)&&(Pionjouer[p].Gety()==j-1)&&(Pionjouer[p].Getcouleur()==op))
                    {
                        int dp=2;
                        while((dp>=2)&&(j-dp>0))
                        {
                            int action=0;
                            for(unsigned int p2=0; p2<Pionjouer.size(); p2++)
                            {
                                if((Pionjouer[p2].Getx()==i)&&(Pionjouer[p2].Gety()==j-dp)&&(Pionjouer[p2].Getcouleur()==op)) //si le pion a droite est de la couleur du joeur oppos� on regarde encore droite
                                {

                                    dp=dp+1;
                                    action=1;

                                }
                                else if((Pionjouer[p2].Getx()==i)&&(Pionjouer[p2].Gety()==j-dp)&&(Pionjouer[p2].Getcouleur()==tour)) //si le pion est de la couleur du jouer on marque
                                {

                                    TabPossibi[i-1][j-1]=1;   //-1 car tableau de 0 a 7 et vecteur de 1 a 8
                                    dp=0;
                                    action=1;

                                }
                            }
                            if(action==0)
                            {
                                dp=0;

                            }



                        }
                    }

                }

                ///verification vers diagonale bas/droite
                for(unsigned int p=0; p<Pionjouer.size(); p++)
                {
                    if((Pionjouer[p].Getx()==i+1)&&(Pionjouer[p].Gety()==j+1)&&(Pionjouer[p].Getcouleur()==tour))
                    {

                        YaPion=1; //futile
                    }
                    else if((Pionjouer[p].Getx()==i+1)&&(Pionjouer[p].Gety()==j+1)&&(Pionjouer[p].Getcouleur()==op))
                    {
                        int dp=2;
                        while((dp>=2)&&(dp+i<=8)&&(dp+j<=8))
                        {
                            int action=0;
                            for(unsigned int p2=0; p2<Pionjouer.size(); p2++)
                            {
                                if((Pionjouer[p2].Getx()==i+dp)&&(Pionjouer[p2].Gety()==j+dp)&&(Pionjouer[p2].Getcouleur()==op)) //si le pion a droite est de la couleur du joeur oppos� on regarde encore droite
                                {

                                    dp=dp+1;
                                    action=1;

                                }
                                else if((Pionjouer[p2].Getx()==i+dp)&&(Pionjouer[p2].Gety()==j+dp)&&(Pionjouer[p2].Getcouleur()==tour)) //si le pion est de la couleur du jouer on marque
                                {

                                    TabPossibi[i-1][j-1]=1;   //-1 car tableau de 0 a 7 et vecteur de 1 a 8
                                    dp=0;
                                    action=1;

                                }
                            }
                            if(action==0)
                            {

                                dp=0;

                            }



                        }
                    }

                }

                ///verification vers diagonale bas/gauche
                for(unsigned int p=0; p<Pionjouer.size(); p++)
                {
                    if((Pionjouer[p].Getx()==i-1)&&(Pionjouer[p].Gety()==j+1)&&(Pionjouer[p].Getcouleur()==tour))
                    {

                        YaPion=1; //futile
                    }
                    else if((Pionjouer[p].Getx()==i-1)&&(Pionjouer[p].Gety()==j+1)&&(Pionjouer[p].Getcouleur()==op))
                    {
                        int dp=2;
                        while((dp>=2)&&(i-dp>0)&&(dp+j<=8))
                        {
                            int action=0;
                            for(unsigned int p2=0; p2<Pionjouer.size(); p2++)
                            {
                                if((Pionjouer[p2].Getx()==i-dp)&&(Pionjouer[p2].Gety()==j+dp)&&(Pionjouer[p2].Getcouleur()==op)) //si le pion a droite est de la couleur du joeur oppos� on regarde encore droite
                                {

                                    dp=dp+1;
                                    action=1;

                                }
                                else if((Pionjouer[p2].Getx()==i-dp)&&(Pionjouer[p2].Gety()==j+dp)&&(Pionjouer[p2].Getcouleur()==tour)) //si le pion est de la couleur du jouer on marque
                                {

                                    TabPossibi[i-1][j-1]=1;   //-1 car tableau de 0 a 7 et vecteur de 1 a 8
                                    dp=0;
                                    action=1;

                                }
                            }
                            if(action==0)
                            {

                                dp=0;

                            }



                        }
                    }

                }

                ///verification vers diagonale haut/gauche
                for(unsigned int p=0; p<Pionjouer.size(); p++)
                {
                    if((Pionjouer[p].Getx()==i-1)&&(Pionjouer[p].Gety()==j-1)&&(Pionjouer[p].Getcouleur()==tour))
                    {

                        YaPion=1; //futile
                    }
                    else if((Pionjouer[p].Getx()==i-1)&&(Pionjouer[p].Gety()==j-1)&&(Pionjouer[p].Getcouleur()==op))
                    {
                        int dp=2;
                        while((dp>=2)&&(i-dp>0)&&(j-dp>0))
                        {
                            int action=0;
                            for(unsigned int p2=0; p2<Pionjouer.size(); p2++)
                            {
                                if((Pionjouer[p2].Getx()==i-dp)&&(Pionjouer[p2].Gety()==j-dp)&&(Pionjouer[p2].Getcouleur()==op)) //si le pion a droite est de la couleur du joeur oppos� on regarde encore droite
                                {

                                    dp=dp+1;
                                    action=1;

                                }
                                else if((Pionjouer[p2].Getx()==i-dp)&&(Pionjouer[p2].Gety()==j-dp)&&(Pionjouer[p2].Getcouleur()==tour)) //si le pion est de la couleur du jouer on marque
                                {

                                    TabPossibi[i-1][j-1]=1;   //-1 car tableau de 0 a 7 et vecteur de 1 a 8
                                    dp=0;
                                    action=1;

                                }
                            }
                            if(action==0)
                            {

                                dp=0;

                            }



                        }
                    }

                }

                ///verificaton vers diagonale haut droite
                for(unsigned int p=0; p<Pionjouer.size(); p++)
                {
                    if((Pionjouer[p].Getx()==i+1)&&(Pionjouer[p].Gety()==j-1)&&(Pionjouer[p].Getcouleur()==tour))
                    {

                        YaPion=1; //futile
                    }
                    else if((Pionjouer[p].Getx()==i+1)&&(Pionjouer[p].Gety()==j-1)&&(Pionjouer[p].Getcouleur()==op))
                    {
                        int dp=2;
                        while((dp>=2)&&(dp+i<=8)&&(j-dp>0))
                        {
                            int action=0;
                            for(unsigned int p2=0; p2<Pionjouer.size(); p2++)
                            {
                                if((Pionjouer[p2].Getx()==i+dp)&&(Pionjouer[p2].Gety()==j-dp)&&(Pionjouer[p2].Getcouleur()==op)) //si le pion a droite est de la couleur du joeur oppos� on regarde encore droite
                                {

                                    dp=dp+1;
                                    action=1;

                                }
                                else if((Pionjouer[p2].Getx()==i+dp)&&(Pionjouer[p2].Gety()==j-dp)&&(Pionjouer[p2].Getcouleur()==tour)) //si le pion est de la couleur du jouer on marque
                                {

                                    TabPossibi[i-1][j-1]=1;   //-1 car tableau de 0 a 7 et vecteur de 1 a 8
                                    dp=0;
                                    action=1;

                                }
                            }
                            if(action==0)
                            {

                                dp=0;

                            }



                        }
                    }

                }













            }
        }
    }





}
<<<<<<< HEAD
void Pion :: sauvegarde()
    {
        //ouverture fichier en mode lecture/�criture
        ofstream save("save1.txt", ios::out | ios::trunc);

        if (save)
        {
            for (unsigned int i=0 ; i<64 ; i++)
            {
                save << Pionjouer[i].m_x;
                save << Pionjouer[i].m_y;
                save << Pionjouer[i].m_couleur;
                save << endl;
            }



            save.close();
        }
        else
            // dit si le fichier existe ou pas
            cout<<"ouverture rate"<< endl;
=======

void  Damier::AfficherPossib(char tour)
{
    Console* pConsole = NULL;
    pConsole = Console::getInstance();
    PossibiJeu(tour);
    int a=0;
    pConsole->Locate(45,6);
    std::cout<<"coup de l'IA possible :";
    pConsole->Locate(50,7);
    std::cout<<"[x] [y]";
    for(int j=0; j<8; j++)
    {

        for(int i=0; i<8; i++)
        {
            if(TabPossibi[i][j]==1)
            {
                pConsole->Locate(50,9+a);
                std::cout<<"["<<i+1<<"] ["<<j+1<<"]";
                a=a+1;
            }
        }
    }
}

void Damier::AfficherDamierAvecAide(char tour)
{
    Console* pConsole = NULL;
    pConsole = Console::getInstance();

    int x=0;
    std::cout<<"     A   B   C   D   E   F   G   H"<<std::endl;
    std::cout<<"   +---+---+---+---+---+---+---+---+"<<std::endl;
    for(int j=0; j<8; j++)    //coordonn�e colonne
    {
        std::cout<<x+1<<"  ";
        for(int i=0; i<8; i++) //coordonn�e ligne
        {
            int paff=0;
            for(unsigned int p=0; p<Pionjouer.size(); p++)
            {

                if((Pionjouer[p].Getx()==i+1)&&(Pionjouer[p].Gety()==j+1))
                {
                    std::cout<<"|";
                    Pionjouer[p].AfficherPion();
                    paff=1;
                    p=Pionjouer.size();

                }
            }
            if(paff==0)
            {
                PossibiJeu(tour);
                if(TabPossibi[i][j]==1)
                {

                    std::cout<<"| ";
                    pConsole->setColor(COLOR_RED);
                    std::cout<<"o ";
                    pConsole->setColor(COLOR_DEFAULT);
                }
                else
                {
                    std::cout<<"|   ";
                }
            }

        }
        std::cout<<"|  "<<x+1<<" ";
        std::cout<<std::endl;
        std::cout<<"   +---+---+---+---+---+---+---+---+  "<<std::endl;
        x=x+1;
    }
    std::cout<<"     A   B   C   D   E   F   G   H"<<std::endl;



}

void Damier::PossibiPoid(char tour)
{



    char op;
    if(tour=='N')
    {
        op='B';
    }
    else if (tour=='B')
    {
        op='N';
    }
    int YaPion=0;
    for(int i=0; i<8; i++)    //initialisation de toute les case a 0
    {
        for(int j=0; j<8; j++)
        {
            tabPoid[i][j]=0;
        }
>>>>>>> 40b6cf161876a08e205139fd4e87722eb4b48ade
    }



<<<<<<< HEAD
void Pion :: lecture()
    {
        ifstream save("save1.txt", ios::in);

        if (save)
        {
            for (unsigned int i=0 ; i<64 ; i++)
            {
                save >> m_x ;
                save >> m_y;
                save >> m_couleur;
                Pionjouer.push_back(Pion(m_couleur, m_x,m_y));
            }



            save.close();
        }
        else
            // dit si le fichier existe ou pas
            cout<<"ouverture rate"<< endl;
    }
=======
    for(int j=1; j<9; j++)
    {


        for(int i=1; i<9; i++)
        {
            YaPion=0;



            for(unsigned int p=0; p<Pionjouer.size(); p++)  //verification presence pion
            {
                if((Pionjouer[p].Getx()==i)&&(Pionjouer[p].Gety()==j)&&(Pionjouer[p].Getcouleur()==tour))
                {

                    YaPion=1;
                }
                else if((Pionjouer[p].Getx()==i)&&(Pionjouer[p].Gety()==j)&&(Pionjouer[p].Getcouleur()==op))
                {
                    YaPion=1;
                }
            }
            if (YaPion==0)
            {
                ///verification possibilit� vers la droite

                for(unsigned int p=0; p<Pionjouer.size(); p++)
                {

                    if((Pionjouer[p].Getx()==i+1)&&(Pionjouer[p].Gety()==j)&&(Pionjouer[p].Getcouleur()==op))
                    {
                        int dp=2;
                        int poid=1;
                        while((dp>=2)&&(dp+i<=8))
                        {

                            int action=0;
                            for(unsigned int p2=0; p2<Pionjouer.size(); p2++)
                            {
                                if((Pionjouer[p2].Getx()==i+dp)&&(Pionjouer[p2].Gety()==j)&&(Pionjouer[p2].Getcouleur()==op)) //si le pion a droite est de la couleur du joeur oppos� on regarde encore droite
                                {
                                    poid++;
                                    dp=dp+1;
                                    action=1;

                                }
                                else if((Pionjouer[p2].Getx()==i+dp)&&(Pionjouer[p2].Gety()==j)&&(Pionjouer[p2].Getcouleur()==tour)) //si le pion est de la couleur du jouer on marque
                                {

                                    tabPoid[i-1][j-1]=tabPoid[i-1][j-1]+poid;   //-1 car tableau de 0 a 7 et vecteur de 1 a 8
                                    dp=0;
                                    action=1;

                                }
                            }
                            if(action==0)
                            {

                                dp=0;

                            }



                        }
                    }

                }
                ///verification possibilit� vers la gauche
                for(unsigned int p=0; p<Pionjouer.size(); p++)
                {

                    if((Pionjouer[p].Getx()==i-1)&&(Pionjouer[p].Gety()==j)&&(Pionjouer[p].Getcouleur()==op))
                    {
                        int dp=2;
                        int poid=1;
                        while((dp>=2)&&(i-dp>0))
                        {
                            int action=0;
                            for(unsigned int p2=0; p2<Pionjouer.size(); p2++)
                            {
                                if((Pionjouer[p2].Getx()==i-dp)&&(Pionjouer[p2].Gety()==j)&&(Pionjouer[p2].Getcouleur()==op)) //si le pion a droite est de la couleur du joeur oppos� on regarde encore droite
                                {
                                    poid++;
                                    dp=dp+1;
                                    action=1;

                                }
                                else if((Pionjouer[p2].Getx()==i-dp)&&(Pionjouer[p2].Gety()==j)&&(Pionjouer[p2].Getcouleur()==tour)) //si le pion est de la couleur du jouer on marque
                                {

                                     tabPoid[i-1][j-1]=tabPoid[i-1][j-1]+poid;   //-1 car tableau de 0 a 7 et vecteur de 1 a 8
                                    dp=0;
                                    action=1;

                                }
                            }
                            if(action==0)
                            {
                                dp=0;

                            }



                        }
                    }

                }

                ///verification vers le bas

                for(unsigned int p=0; p<Pionjouer.size(); p++)
                {
                    if((Pionjouer[p].Getx()==i)&&(Pionjouer[p].Gety()==j+1)&&(Pionjouer[p].Getcouleur()==op))
                    {
                        int dp=2;
                        int poid=1;
                        while((dp>=2)&&(dp+j<=8))
                        {
                            int action=0;
                            for(unsigned int p2=0; p2<Pionjouer.size(); p2++)
                            {
                                if((Pionjouer[p2].Getx()==i)&&(Pionjouer[p2].Gety()==j+dp)&&(Pionjouer[p2].Getcouleur()==op)) //si le pion a droite est de la couleur du joeur oppos� on regarde encore droite
                                {
                                    poid++;
                                    dp=dp+1;
                                    action=1;

                                }
                                else if((Pionjouer[p2].Getx()==i)&&(Pionjouer[p2].Gety()==j+dp)&&(Pionjouer[p2].Getcouleur()==tour)) //si le pion est de la couleur du jouer on marque
                                {

                                    tabPoid[i-1][j-1]=tabPoid[i-1][j-1]+poid;  //-1 car tableau de 0 a 7 et vecteur de 1 a 8
                                    dp=0;
                                    action=1;

                                }
                            }
                            if(action==0)
                            {

                                dp=0;

                            }



                        }
                    }

                }
                ///verification vers le haut
                for(unsigned int p=0; p<Pionjouer.size(); p++)
                {
                    if((Pionjouer[p].Getx()==i)&&(Pionjouer[p].Gety()==j-1)&&(Pionjouer[p].Getcouleur()==tour))
                    {

                        YaPion=1;  //futile
                    }
                    else if((Pionjouer[p].Getx()==i)&&(Pionjouer[p].Gety()==j-1)&&(Pionjouer[p].Getcouleur()==op))
                    {
                        int dp=2;
                        int poid=1;
                        while((dp>=2)&&(j-dp>0))
                        {
                            int action=0;
                            for(unsigned int p2=0; p2<Pionjouer.size(); p2++)
                            {
                                if((Pionjouer[p2].Getx()==i)&&(Pionjouer[p2].Gety()==j-dp)&&(Pionjouer[p2].Getcouleur()==op)) //si le pion a droite est de la couleur du joeur oppos� on regarde encore droite
                                {
                                    poid++;
                                    dp=dp+1;
                                    action=1;

                                }
                                else if((Pionjouer[p2].Getx()==i)&&(Pionjouer[p2].Gety()==j-dp)&&(Pionjouer[p2].Getcouleur()==tour)) //si le pion est de la couleur du jouer on marque
                                {

                                     tabPoid[i-1][j-1]=tabPoid[i-1][j-1]+poid;   //-1 car tableau de 0 a 7 et vecteur de 1 a 8
                                    dp=0;
                                    action=1;

                                }
                            }
                            if(action==0)
                            {
                                dp=0;

                            }



                        }
                    }

                }

                ///verification vers diagonale bas/droite
                for(unsigned int p=0; p<Pionjouer.size(); p++)
                {
                    if((Pionjouer[p].Getx()==i+1)&&(Pionjouer[p].Gety()==j+1)&&(Pionjouer[p].Getcouleur()==tour))
                    {

                        YaPion=1; //futile
                    }
                    else if((Pionjouer[p].Getx()==i+1)&&(Pionjouer[p].Gety()==j+1)&&(Pionjouer[p].Getcouleur()==op))
                    {
                        int dp=2;
                        int poid=1;
                        while((dp>=2)&&(dp+i<=8)&&(dp+j<=8))
                        {
                            int action=0;
                            for(unsigned int p2=0; p2<Pionjouer.size(); p2++)
                            {
                                if((Pionjouer[p2].Getx()==i+dp)&&(Pionjouer[p2].Gety()==j+dp)&&(Pionjouer[p2].Getcouleur()==op)) //si le pion a droite est de la couleur du joeur oppos� on regarde encore droite
                                {
                                    poid++;
                                    dp=dp+1;
                                    action=1;

                                }
                                else if((Pionjouer[p2].Getx()==i+dp)&&(Pionjouer[p2].Gety()==j+dp)&&(Pionjouer[p2].Getcouleur()==tour)) //si le pion est de la couleur du jouer on marque
                                {

                                     tabPoid[i-1][j-1]=tabPoid[i-1][j-1]+poid;   //-1 car tableau de 0 a 7 et vecteur de 1 a 8
                                    dp=0;
                                    action=1;

                                }
                            }
                            if(action==0)
                            {

                                dp=0;

                            }



                        }
                    }

                }

                ///verification vers diagonale bas/gauche
                for(unsigned int p=0; p<Pionjouer.size(); p++)
                {
                    if((Pionjouer[p].Getx()==i-1)&&(Pionjouer[p].Gety()==j+1)&&(Pionjouer[p].Getcouleur()==tour))
                    {

                        YaPion=1; //futile
                    }
                    else if((Pionjouer[p].Getx()==i-1)&&(Pionjouer[p].Gety()==j+1)&&(Pionjouer[p].Getcouleur()==op))
                    {
                        int dp=2;
                        int poid=1;
                        while((dp>=2)&&(i-dp>0)&&(dp+j<=8))
                        {
                            int action=0;
                            for(unsigned int p2=0; p2<Pionjouer.size(); p2++)
                            {
                                if((Pionjouer[p2].Getx()==i-dp)&&(Pionjouer[p2].Gety()==j+dp)&&(Pionjouer[p2].Getcouleur()==op)) //si le pion a droite est de la couleur du joeur oppos� on regarde encore droite
                                {
                                    poid++;
                                    dp=dp+1;
                                    action=1;

                                }
                                else if((Pionjouer[p2].Getx()==i-dp)&&(Pionjouer[p2].Gety()==j+dp)&&(Pionjouer[p2].Getcouleur()==tour)) //si le pion est de la couleur du jouer on marque
                                {

                                     tabPoid[i-1][j-1]=tabPoid[i-1][j-1]+poid;  //-1 car tableau de 0 a 7 et vecteur de 1 a 8
                                    dp=0;
                                    action=1;

                                }
                            }
                            if(action==0)
                            {

                                dp=0;

                            }



                        }
                    }

                }

                ///verification vers diagonale haut/gauche
                for(unsigned int p=0; p<Pionjouer.size(); p++)
                {
                    if((Pionjouer[p].Getx()==i-1)&&(Pionjouer[p].Gety()==j-1)&&(Pionjouer[p].Getcouleur()==tour))
                    {

                        YaPion=1; //futile
                    }
                    else if((Pionjouer[p].Getx()==i-1)&&(Pionjouer[p].Gety()==j-1)&&(Pionjouer[p].Getcouleur()==op))
                    {
                        int dp=2;
                        int poid=1;
                        while((dp>=2)&&(i-dp>0)&&(j-dp>0))
                        {
                            int action=0;
                            for(unsigned int p2=0; p2<Pionjouer.size(); p2++)
                            {
                                if((Pionjouer[p2].Getx()==i-dp)&&(Pionjouer[p2].Gety()==j-dp)&&(Pionjouer[p2].Getcouleur()==op)) //si le pion a droite est de la couleur du joeur oppos� on regarde encore droite
                                {

                                    dp=dp+1;
                                    poid++;
                                    action=1;

                                }
                                else if((Pionjouer[p2].Getx()==i-dp)&&(Pionjouer[p2].Gety()==j-dp)&&(Pionjouer[p2].Getcouleur()==tour)) //si le pion est de la couleur du jouer on marque
                                {

                                     tabPoid[i-1][j-1]=tabPoid[i-1][j-1]+poid;  //-1 car tableau de 0 a 7 et vecteur de 1 a 8
                                    dp=0;
                                    action=1;

                                }
                            }
                            if(action==0)
                            {

                                dp=0;

                            }



                        }
                    }

                }

                ///verificaton vers diagonale haut droite
                for(unsigned int p=0; p<Pionjouer.size(); p++)
                {
                    if((Pionjouer[p].Getx()==i+1)&&(Pionjouer[p].Gety()==j-1)&&(Pionjouer[p].Getcouleur()==tour))
                    {

                        YaPion=1; //futile
                    }
                    else if((Pionjouer[p].Getx()==i+1)&&(Pionjouer[p].Gety()==j-1)&&(Pionjouer[p].Getcouleur()==op))
                    {
                        int dp=2;
                        int poid=1;
                        while((dp>=2)&&(dp+i<=8)&&(j-dp>0))
                        {
                            int action=0;
                            for(unsigned int p2=0; p2<Pionjouer.size(); p2++)
                            {
                                if((Pionjouer[p2].Getx()==i+dp)&&(Pionjouer[p2].Gety()==j-dp)&&(Pionjouer[p2].Getcouleur()==op)) //si le pion a droite est de la couleur du joeur oppos� on regarde encore droite
                                {

                                    dp=dp+1;
                                    poid++;
                                    action=1;

                                }
                                else if((Pionjouer[p2].Getx()==i+dp)&&(Pionjouer[p2].Gety()==j-dp)&&(Pionjouer[p2].Getcouleur()==tour)) //si le pion est de la couleur du jouer on marque
                                {

                                    tabPoid[i-1][j-1]=tabPoid[i-1][j-1]+poid;   //-1 car tableau de 0 a 7 et vecteur de 1 a 8
                                    dp=0;
                                    action=1;

                                }
                            }
                            if(action==0)
                            {

                                dp=0;

                            }



                        }
                    }

                }













            }
        }
    }




}


void  Damier::AfficherPoid(char tour)
{
    Console* pConsole = NULL;
    pConsole = Console::getInstance();
    PossibiPoid(tour);
    int a=0;
    pConsole->Locate(45,6);
    std::cout<<"coup de l'IA possible :";
    pConsole->Locate(50,7);
    std::cout<<"[x] [y]";
    for(int j=0; j<8; j++)
    {

        for(int i=0; i<8; i++)
        {
            if(tabPoid[i][j]>=1)
            {
                pConsole->Locate(50,9+a);
                std::cout<<"["<<i+1<<"] ["<<j+1<<"]  --> "<<tabPoid[i][j];
                a=a+1;
            }
        }
    }
}

>>>>>>> 40b6cf161876a08e205139fd4e87722eb4b48ade
