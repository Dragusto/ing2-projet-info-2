#ifndef DAMIER_H
#define DAMIER_H
#include <vector>
#include <Pion.h>
#include <iostream>
#include <Damier.h>

class Damier
{
public:
    Damier();
    virtual ~Damier();
    void AfficherDamier();
    void AjouterPion(char Couleur,int x,int y);
    void PossibiJeu(char Tour);
    void AfficherPossib(char tour);
    void AfficherPoid(char tour);
    void PossibiPoid(char tour);
    int tabPoid[8][8];            //tableau de poid
    int TabPossibi[8][8];         //tableau de marquage des casse possible
    void AfficherDamierAvecAide(char tour);
    std::vector<Pion> Pionjouer;

protected:

private:


};

#endif // DAMIER_H
