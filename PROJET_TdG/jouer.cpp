#include <jouer.h>
#include <Damier.h>
#include <Pion.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <ctime>
#include <cstdlib>
#include <stdlib.h>
#include <console.h>
#include <conio.h>

jouer::jouer()
{
    //ctor
}

jouer::~jouer()
{
    //dtor
}

void jouer::jouerJvsJ()
{

    Damier damier;
    //damier.PossibiJeu('N');  ///modifier

    int possib=1;
    int tour=1;
    char joueur;
    int nbN=0;
    int nbB=0;
    int pionjouer=nbN+nbB;


    //curseur
    Console* pConsole = NULL;
    pConsole = Console::getInstance();
    int curX=5;
    int curY=3;
    int x=1;
    int y=1;





        while((possib!=0)&&(pionjouer<64))
        {
            char choix='0';






            nbN=0;
            nbB=0;
            if(tour==1)   //obtenir couleur joueur
            {
                joueur='N';
            }
            if(tour==-1)  //obtenir couleur joueur
            {
                joueur='B';
            }

            //determiner nb pion jouer
            for(unsigned int i=0; i<damier.Pionjouer.size(); i++)
            {
                if(damier.Pionjouer[i].Getcouleur()=='N')
                {
                    nbN=nbN+1;
                }
                if(damier.Pionjouer[i].Getcouleur()=='B')
                {
                    nbB=nbB+1;
                }

            }
            damier.PossibiJeu(joueur);



            //obtenir nb possibilité
            possib=0;
            for(int i=0; i<8; i++)
            {

                for(int j=0; j<8; j++)
                {
                    if(damier.TabPossibi[i][j]==1)
                    {
                        possib=possib+1;
                    }
                }
            }


            system("cls");
            std::cout<<"         c'est au "<<joueur<<" de jouer "<<std::endl;
            damier.AfficherDamier();
            std::cout<<"Noir : "<<nbN<<" - "<<nbB<<" Blanc"<<std::endl;
            pConsole->Locate(50,5);
            std::cout<<"curX :"<<curX<<"  curY :"<<curY<<std::endl;
            pConsole->Locate(50,6);
            std::cout<<"X :"<<x<<" Y :"<<y;
            pConsole->Locate(curX,curY);




            while((choix!='s')&&(choix!='z')&&(choix!=' ')&&(choix!='q')&&(choix!='d'))
            {


                pConsole->Locate(curX,curY);

                choix=getch();

            }

            switch(choix)
            {
            case 's':
            {
                if(curY<17)
                {
                    curY=curY+2;
                    y=y+1;
                }
                break;

            }
            case 'z':
            {
                if(curY>3)
                {
                    curY=curY-2;
                    y=y-1;
                }
                break;
            }
            case 'd':
            {
                if(curX<31)
                {
                    curX=curX+4;
                    x=x+1;

                }
                break;

            }
            case 'q':
            {
                if(curX>5)
                {
                    curX=curX-4;
                    x=x-1;
                }
                break;
            }
            case ' ':
            {
                if(damier.TabPossibi[x-1][y-1]==1)
                {
                    damier.AjouterPion(joueur,x,y);
                    tour=tour*(-1);
                    break;
                }

            }
            }


        }



system("cls");
pConsole->Locate(15,10);
    if(nbN>nbB)
    {
        std::cout<<"LES NOIRS ONT GAGNES !!!!"<<std::endl;
         std::cout<<" avec "<<nbN<<" pions contre "<<nbB;
    }
    else if(nbB>nbN)
    {
        std::cout<<"LES BLANCS ONT GAGNES !!!!"<<std::endl;
         std::cout<<" avec "<<nbB<<" pions contre "<<nbN;
    }
    else if(nbN==nbB)
    {
        std::cout<<"EGALITE"<<std::endl;
    }


    getch();

}

void jouer::jouerJvsJavecaide()
{

    Damier damier;

    int possib=1;
    int tour=1;
    char joueur;
    int nbN=0;
    int nbB=0;
    int pionjouer=nbN+nbB;


    //curseur
    Console* pConsole = NULL;
    pConsole = Console::getInstance();
    int curX=5;
    int curY=3;
    int x=1;
    int y=1;





        while((possib!=0)&&(pionjouer!=64))
        {
            char choix='0';






            nbN=0;
            nbB=0;
            if(tour==1)   //obtenir couleur joueur
            {
                joueur='N';
            }
            if(tour==-1)  //obtenir couleur joueur
            {
                joueur='B';
            }

            //determiner nb pion jouer
            for(unsigned int i=0; i<damier.Pionjouer.size(); i++)
            {
                if(damier.Pionjouer[i].Getcouleur()=='N')
                {
                    nbN=nbN+1;
                }
                if(damier.Pionjouer[i].Getcouleur()=='B')
                {
                    nbB=nbB+1;
                }

            }
            damier.PossibiJeu(joueur);



            //obtenir nb possibilité
            possib=0;
            for(int i=0; i<8; i++)
            {

                for(int j=0; j<8; j++)
                {
                    if(damier.TabPossibi[i][j]==1)
                    {
                        possib=possib+1;
                    }
                }
            }

            system("cls");
            std::cout<<"         c'est au "<<joueur<<" de jouer "<<std::endl;
            damier.AfficherDamierAvecAide(joueur);
            std::cout<<"Noir : "<<nbN<<" - "<<nbB<<" : Blanc"<<std::endl;
            pConsole->Locate(50,5);
            std::cout<<"curX :"<<curX<<"  curY :"<<curY<<std::endl;
            pConsole->Locate(50,6);
            std::cout<<"X :"<<x<<" Y :"<<y;
            pConsole->Locate(curX,curY);




            while((choix!='s')&&(choix!='z')&&(choix!=' ')&&(choix!='q')&&(choix!='d'))
            {


                pConsole->Locate(curX,curY);

                choix=getch();

            }

            switch(choix)
            {
            case 's':
            {
                if(curY<17)
                {
                    curY=curY+2;
                    y=y+1;
                }
                break;

            }
            case 'z':
            {
                if(curY>3)
                {
                    curY=curY-2;
                    y=y-1;
                }
                break;
            }
            case 'd':
            {
                if(curX<31)
                {
                    curX=curX+4;
                    x=x+1;

                }
                break;

            }
            case 'q':
            {
                if(curX>5)
                {
                    curX=curX-4;
                    x=x-1;
                }
                break;
            }
            case ' ':
            {
                if(damier.TabPossibi[x-1][y-1]==1)
                {
                    damier.AjouterPion(joueur,x,y);
                    tour=tour*(-1);
                    break;
                }

            }
            }


        }



    system("cls");
    pConsole->Locate(15,10);

    if(nbN>nbB)
    {
        std::cout<<"LES NOIRS ONT GAGNES !!!!"<<std::endl;
         std::cout<<" avec "<<nbN<<" pions contre "<<nbB;
    }
    else if(nbB>nbN)
    {
        std::cout<<"LES BLANCS ONT GAGNES !!!!"<<std::endl;
         std::cout<<" avec "<<nbB<<" pions contre "<<nbN;
    }
    else if(nbN==nbB)
    {
        std::cout<<"EGALITE"<<std::endl;
    }


    getch();

}


void jouer::jouerIAalea(char jr)
{
    Console* pConsole = NULL;
    pConsole = Console::getInstance();
    int curX=5;
    int curY=3;
    int x=1;
    int y=1;

    srand(time(NULL));

    Damier damier;
    int possib=1;
    int tour=1;
    char joueur;
    int nbN=0;
    int nbB=0;
    int pionjouer=nbN+nbB;


    while((possib!=0)&&(pionjouer<64))
    {
        char IA;
        if(jr=='B')
        {
            IA='N';
        }
        if(jr=='N')
        {
            IA='B';
        }

        char choix='0';


        // damier.AfficherDamier();




        nbN=0;
        nbB=0;
        if(tour==1)
        {
            joueur='N';
        }
        if(tour==-1)
        {
            joueur='B';
        }

        damier.PossibiJeu(joueur);


        for(unsigned int i=0; i<damier.Pionjouer.size(); i++)
        {
            if(damier.Pionjouer[i].Getcouleur()=='N')
            {
                nbN=nbN+1;
            }
            if(damier.Pionjouer[i].Getcouleur()=='B')
            {
                nbB=nbB+1;
            }

        }

        system("cls");
        std::cout<<"         c'est au "<<joueur<<" de jouer "<<std::endl;
        damier.AfficherDamier();
        std::cout<<"Noir : "<<nbN<<" - "<<nbB<<" Blanc"<<std::endl;
        pConsole->Locate(50,5);
        std::cout<<"curX :"<<curX<<"  curY :"<<curY<<std::endl;
        pConsole->Locate(50,6);
        std::cout<<"X :"<<x<<" Y :"<<y;
        pConsole->Locate(curX,curY);





        //calcul possib
        possib=0;
        for(int i=0; i<8; i++)
        {
            for(int j=0; j<8; j++)
            {
                if(damier.TabPossibi[i][j]!=0)
                {
                    possib=possib+1;
                }
            }
        }

        if(jr==joueur)
        {
            while((choix!='s')&&(choix!='z')&&(choix!=' ')&&(choix!='q')&&(choix!='d'))
            {
                // system("cls");

                pConsole->Locate(curX,curY);

                choix=getch();

            }

            switch(choix)
            {
            case 's':
            {
                if(curY<17)
                {
                    curY=curY+2;
                    y=y+1;
                }
                break;

            }
            case 'z':
            {
                if(curY>3)
                {
                    curY=curY-2;
                    y=y-1;
                }
                break;
            }
            case 'd':
            {
                if(curX<31)
                {
                    curX=curX+4;
                    x=x+1;

                }
                break;

            }
            case 'q':
            {
                if(curX>5)
                {
                    curX=curX-4;
                    x=x-1;
                }
                break;
            }
            case ' ':
            {
                if(damier.TabPossibi[x-1][y-1]>=1)
                {
                    damier.AjouterPion(joueur,x,y);
                    tour=tour*(-1);
                    break;
                }

            }

            }
        }
        else
        {
            char p='0';

            system("cls");
            std::cout<<"         c'est au "<<joueur<<" de jouer "<<std::endl;
            damier.AfficherDamier();
            std::cout<<"Noir : "<<nbN<<" - "<<nbB<<" Blanc"<<std::endl;
            pConsole->Locate(40,5);

            std::cout<<"appuyer sur une touche pour faire jouer l'IA";
            pConsole->Locate(40,4);

            std::cout<<"appuyer sur 'c' pour afficher l'arbre des possibilitee de l'IA";
            pConsole->Locate(curX,curY);
            p=getch();


            if(p=='c')
            {
                system("cls");
                std::cout<<"         c'est au "<<joueur<<" de jouer "<<std::endl;
                damier.AfficherDamierAvecAide(joueur);
                std::cout<<"Noir : "<<nbN<<" - "<<nbB<<" Blanc"<<std::endl;
                damier.AfficherPossib(IA);
                getch();
            }




            int Xia=8;
            int Yia=8;
            while(damier.TabPossibi[Xia-1][Yia-1]==0)
            {
                Xia=rand()%(7)+1;
                Yia=rand()%(7)+1;
            }
            system("cls");

            damier.AjouterPion(joueur,Xia,Yia);
            tour=tour*(-1);


            getch();


        }




    }
   system("cls");
   pConsole->Locate(15,10);
    if(nbN>nbB)
    {
        std::cout<<"LES NOIRS ONT GAGNES !!!!"<<std::endl;
         std::cout<<" avec "<<nbN<<" pions contre "<<nbB;
    }
    else if(nbB>nbN)
    {
        std::cout<<"LES BLANCS ONT GAGNES !!!!"<<std::endl;
         std::cout<<" avec "<<nbB<<" pions contre "<<nbN;
    }
    else if(nbN==nbB)
    {
        std::cout<<"EGALITE"<<std::endl;
    }

    getch();


}

void jouer::jouerIAaleaavecaide(char jr)
{
    Console* pConsole = NULL;
    pConsole = Console::getInstance();
    int curX=5;
    int curY=3;
    int x=1;
    int y=1;

    srand(time(NULL));

    Damier damier;
    int possib=1;
    int tour=1;
    char joueur;
    int nbN=0;
    int nbB=0;
    int pionjouer=nbN+nbB;


    while((possib!=0)&&(pionjouer!=64))
    {
        char IA;
        if(jr=='B')
        {
            IA='N';
        }
        if(jr=='N')
        {
            IA='B';
        }

        char choix='0';






        possib=1;
        nbN=0;
        nbB=0;
        if(tour==1)
        {
            joueur='N';
        }
        if(tour==-1)
        {
            joueur='B';
        }

        damier.PossibiJeu(joueur);


        for(unsigned int i=0; i<damier.Pionjouer.size(); i++)
        {
            if(damier.Pionjouer[i].Getcouleur()=='N')
            {
                nbN=nbN+1;
            }
            if(damier.Pionjouer[i].Getcouleur()=='B')
            {
                nbB=nbB+1;
            }

        }

        system("cls");
        std::cout<<"         c'est au "<<joueur<<" de jouer "<<std::endl;


        if(joueur==jr)
        {
            damier.AfficherDamierAvecAide(joueur);
        }
        else
        {
            damier.AfficherDamier();
        }
        std::cout<<"Noir : "<<nbN<<" - "<<nbB<<" Blanc"<<std::endl;
        pConsole->Locate(50,5);
        std::cout<<"curX :"<<curX<<"  curY :"<<curY<<std::endl;
        pConsole->Locate(50,6);
        std::cout<<"X :"<<x<<" Y :"<<y;
        pConsole->Locate(curX,curY);





        //calcul possib
        possib=0;
        for(int i=0; i<8; i++)
        {
            for(int j=0; j<8; j++)
            {
                if(damier.TabPossibi[i][j]==1)
                {
                    possib=possib+1;
                }
            }
        }

        if(jr==joueur)
        {
            while((choix!='s')&&(choix!='z')&&(choix!=' ')&&(choix!='q')&&(choix!='d'))
            {


                pConsole->Locate(curX,curY);

                choix=getch();

            }

            switch(choix)
            {
            case 's':
            {
                if(curY<17)
                {
                    curY=curY+2;
                    y=y+1;
                }
                break;

            }
            case 'z':
            {
                if(curY>3)
                {
                    curY=curY-2;
                    y=y-1;
                }
                break;
            }
            case 'd':
            {
                if(curX<31)
                {
                    curX=curX+4;
                    x=x+1;

                }
                break;

            }
            case 'q':
            {
                if(curX>5)
                {
                    curX=curX-4;
                    x=x-1;
                }
                break;
            }
            case ' ':
            {
                if(damier.TabPossibi[x-1][y-1]==1)
                {
                    damier.AjouterPion(joueur,x,y);
                    tour=tour*(-1);
                    break;
                }

            }

            }
        }
        else
        {
            char p='0';


            system("cls");
            std::cout<<"         c'est au "<<joueur<<" de jouer "<<std::endl;
            damier.AfficherDamier();
            std::cout<<"Noir : "<<nbN<<" - "<<nbB<<" Blanc"<<std::endl;
            pConsole->Locate(40,5);

            std::cout<<"appuyer sur une touche pour faire jouer l'IA";
            pConsole->Locate(40,4);

            std::cout<<"appuyer sur 'c' pour afficher l'arbre des possibilitee de l'IA";
            pConsole->Locate(curX,curY);
            p=getch();


            if(p=='c')
            {
                system("cls");
                std::cout<<"         c'est au "<<joueur<<" de jouer "<<std::endl;
                damier.AfficherDamierAvecAide(joueur);
                std::cout<<"Noir : "<<nbN<<" - "<<nbB<<" Blanc"<<std::endl;
                damier.AfficherPossib(IA);
                getch();
            }




            int Xia=0;
            int Yia=0;
            while((damier.TabPossibi[Xia-1][Yia-1]!=1))
            {
                Xia=rand()%(7)+1;
                Yia=rand()%(7)+1;
            }

            damier.AjouterPion(joueur,Xia,Yia);
            tour=tour*(-1);




        }




    }
system("cls");
pConsole->Locate(15,10);
    if(nbN>nbB)
    {
        std::cout<<"LES NOIRS ONT GAGNES !!!!"<<std::endl;
         std::cout<<" avec "<<nbN<<" pions contre "<<nbB;
    }
    else if(nbB>nbN)
    {
        std::cout<<"LES BLANCS ONT GAGNES !!!!"<<std::endl;
         std::cout<<" avec "<<nbB<<" pions contre "<<nbN;
    }
    else if(nbN==nbB)
    {
        std::cout<<"EGALITE"<<std::endl;
    }

    getch();


}



void jouer::jouerIAniv1(char jr)
{
    Console* pConsole = NULL;
    pConsole = Console::getInstance();
    int curX=5;
    int curY=3;
    int x=1;
    int y=1;

    srand(time(NULL));

    Damier damier;
    int possib=1;
    int tour=1;
    char joueur;
    int nbN=0;
    int nbB=0;
    int pionjouer=nbN+nbB;


    while((possib!=0)&&(pionjouer<64))
    {
        char IA;
        if(jr=='B')
        {
            IA='N';
        }
        if(jr=='N')
        {
            IA='B';
        }

        char choix='0';


        // damier.AfficherDamier();




        nbN=0;
        nbB=0;
        if(tour==1)
        {
            joueur='N';
        }
        if(tour==-1)
        {
            joueur='B';
        }

        damier.PossibiPoid(joueur);


        for(unsigned int i=0; i<damier.Pionjouer.size(); i++)
        {
            if(damier.Pionjouer[i].Getcouleur()=='N')
            {
                nbN=nbN+1;
            }
            if(damier.Pionjouer[i].Getcouleur()=='B')
            {
                nbB=nbB+1;
            }

        }

        system("cls");
        std::cout<<"         c'est au "<<joueur<<" de jouer "<<std::endl;
        damier.AfficherDamier();
        std::cout<<"Noir : "<<nbN<<" - "<<nbB<<" Blanc"<<std::endl;
        pConsole->Locate(50,5);
        std::cout<<"curX :"<<curX<<"  curY :"<<curY<<std::endl;
        pConsole->Locate(50,6);
        std::cout<<"X :"<<x<<" Y :"<<y;
        pConsole->Locate(curX,curY);





        //calcul possib
        possib=0;
        for(int i=0; i<8; i++)
        {
            for(int j=0; j<8; j++)
            {
                if(damier.tabPoid[i][j]!=0)
                {
                    possib=possib+1;
                }
            }
        }

        if(jr==joueur)
        {
            while((choix!='s')&&(choix!='z')&&(choix!=' ')&&(choix!='q')&&(choix!='d'))
            {
                // system("cls");

                pConsole->Locate(curX,curY);

                choix=getch();

            }

            switch(choix)
            {
            case 's':
            {
                if(curY<17)
                {
                    curY=curY+2;
                    y=y+1;
                }
                break;

            }
            case 'z':
            {
                if(curY>3)
                {
                    curY=curY-2;
                    y=y-1;
                }
                break;
            }
            case 'd':
            {
                if(curX<31)
                {
                    curX=curX+4;
                    x=x+1;

                }
                break;

            }
            case 'q':
            {
                if(curX>5)
                {
                    curX=curX-4;
                    x=x-1;
                }
                break;
            }
            case ' ':
            {
                if(damier.tabPoid[x-1][y-1]>=1)
                {
                    damier.AjouterPion(joueur,x,y);
                    tour=tour*(-1);
                    break;
                }

            }

            }
        }
        else
        {
            char p='0';

            system("cls");
            std::cout<<"         c'est au "<<joueur<<" de jouer "<<std::endl;
            damier.AfficherDamier();
            std::cout<<"Noir : "<<nbN<<" - "<<nbB<<" Blanc"<<std::endl;
            pConsole->Locate(40,5);

            std::cout<<"appuyer sur une touche pour faire jouer l'IA";
            pConsole->Locate(40,4);

            std::cout<<"appuyer sur 'c' pour afficher l'arbre des possibilitee de l'IA";
            pConsole->Locate(curX,curY);
            p=getch();


            if(p=='c')
            {
                system("cls");
                std::cout<<"         c'est au "<<joueur<<" de jouer "<<std::endl;
                damier.AfficherDamierAvecAide(joueur);
                std::cout<<"Noir : "<<nbN<<" - "<<nbB<<" Blanc"<<std::endl;
                damier.AfficherPoid(IA);
                getch();
            }




            int Max=0;


            damier.PossibiPoid(joueur);
            for(int j=0;j<8;j++)
            {
                for (int i=0;i<8;i++)
                {
                    if(damier.tabPoid[i][j]>Max)
                    {
                        Max=damier.tabPoid[i][j];
                    }

                }
            }
            int Xia=8;
            int Yia=8;
            while(damier.tabPoid[Xia-1][Yia-1]!=Max)
            {
                Xia=rand()%(7)+1;
                Yia=rand()%(7)+1;
            }
            system("cls");

            damier.AjouterPion(joueur,Xia,Yia);

            tour=tour*(-1);


            getch();


        }




    }
   system("cls");
   pConsole->Locate(15,10);
    if(nbN>nbB)
    {
        std::cout<<"LES NOIRS ONT GAGNES !!!!"<<std::endl;
        std::cout<<" avec "<<nbN<<" pions contre "<<nbB;
    }
    else if(nbB>nbN)
    {
        std::cout<<"LES BLANCS ONT GAGNES !!!!"<<std::endl;
         std::cout<<" avec "<<nbB<<" pions contre "<<nbN;
    }
    else if(nbN==nbB)
    {
        std::cout<<"EGALITE"<<std::endl;
    }

    getch();










}



void jouer::jouerIAniv1avecaide(char jr)
{
    Console* pConsole = NULL;
    pConsole = Console::getInstance();
    int curX=5;
    int curY=3;
    int x=1;
    int y=1;

    srand(time(NULL));

    Damier damier;
    int possib=1;
    int tour=1;
    char joueur;
    int nbN=0;
    int nbB=0;
    int pionjouer=nbN+nbB;


    while((possib!=0)&&(pionjouer!=64))
    {
        char IA;
        if(jr=='B')
        {
            IA='N';
        }
        if(jr=='N')
        {
            IA='B';
        }

        char choix='0';






        possib=1;
        nbN=0;
        nbB=0;
        if(tour==1)
        {
            joueur='N';
        }
        if(tour==-1)
        {
            joueur='B';
        }

        damier.PossibiPoid(joueur);


        for(unsigned int i=0; i<damier.Pionjouer.size(); i++)
        {
            if(damier.Pionjouer[i].Getcouleur()=='N')
            {
                nbN=nbN+1;
            }
            if(damier.Pionjouer[i].Getcouleur()=='B')
            {
                nbB=nbB+1;
            }

        }

        system("cls");
        std::cout<<"         c'est au "<<joueur<<" de jouer "<<std::endl;


        if(joueur==jr)
        {
            damier.AfficherDamierAvecAide(joueur);
        }
        else
        {
            damier.AfficherDamier();
        }
        std::cout<<"Noir : "<<nbN<<" - "<<nbB<<" Blanc"<<std::endl;
        pConsole->Locate(50,5);
        std::cout<<"curX :"<<curX<<"  curY :"<<curY<<std::endl;
        pConsole->Locate(50,6);
        std::cout<<"X :"<<x<<" Y :"<<y;
        pConsole->Locate(curX,curY);





        //calcul possib
        possib=0;
        for(int i=0; i<8; i++)
        {
            for(int j=0; j<8; j++)
            {
                if(damier.tabPoid[i][j]>0)
                {
                    possib=possib+1;
                }
            }
        }

        if(jr==joueur)
        {
            while((choix!='s')&&(choix!='z')&&(choix!=' ')&&(choix!='q')&&(choix!='d'))
            {


                pConsole->Locate(curX,curY);

                choix=getch();

            }

            switch(choix)
            {
            case 's':
            {
                if(curY<17)
                {
                    curY=curY+2;
                    y=y+1;
                }
                break;

            }
            case 'z':
            {
                if(curY>3)
                {
                    curY=curY-2;
                    y=y-1;
                }
                break;
            }
            case 'd':
            {
                if(curX<31)
                {
                    curX=curX+4;
                    x=x+1;

                }
                break;

            }
            case 'q':
            {
                if(curX>5)
                {
                    curX=curX-4;
                    x=x-1;
                }
                break;
            }
            case ' ':
            {
                if(damier.tabPoid[x-1][y-1]>0)
                {
                    damier.AjouterPion(joueur,x,y);
                    tour=tour*(-1);
                    break;
                }

            }

            }
        }
        else
        {
            char p='0';


            system("cls");
            std::cout<<"         c'est au "<<joueur<<" de jouer "<<std::endl;
            damier.AfficherDamier();
            std::cout<<"Noir : "<<nbN<<" - "<<nbB<<" Blanc"<<std::endl;
            pConsole->Locate(40,5);

            std::cout<<"appuyer sur une touche pour faire jouer l'IA";
            pConsole->Locate(40,4);

            std::cout<<"appuyer sur 'c' pour afficher l'arbre des possibilité de l'IA";
            pConsole->Locate(curX,curY);
            p=getch();


            if(p=='c')
            {
                system("cls");
                std::cout<<"         c'est au "<<joueur<<" de jouer "<<std::endl;
                damier.AfficherDamierAvecAide(joueur);
                std::cout<<"Noir : "<<nbN<<" - "<<nbB<<" Blanc"<<std::endl;
                damier.AfficherPoid(IA);
                getch();
            }




             int Max=0;


            damier.PossibiPoid(joueur);
            for(int j=0;j<8;j++)
            {
                for (int i=0;i<8;i++)
                {
                    if(damier.tabPoid[i][j]>Max)
                    {
                        Max=damier.tabPoid[i][j];
                    }

                }
            }
            int Xia=8;
            int Yia=8;
            while(damier.tabPoid[Xia-1][Yia-1]!=Max)
            {
                Xia=rand()%(7)+1;
                Yia=rand()%(7)+1;
            }
            damier.AjouterPion(joueur,Xia,Yia);
            tour=tour*(-1);




        }




    }
system("cls");
pConsole->Locate(15,10);
    if(nbN>nbB)
    {
        std::cout<<"LES NOIRS ONT GAGNES !!!!"<<std::endl;
         std::cout<<" avec "<<nbN<<" pions contre "<<nbB;
    }
    else if(nbB>nbN)
    {
        std::cout<<"LES BLANCS ONT GAGNES !!!!"<<std::endl;
         std::cout<<" avec "<<nbB<<" pions contre "<<nbN;
    }
    else if(nbN==nbB)
    {
        std::cout<<"EGALITE"<<std::endl;
    }

    getch();


}
