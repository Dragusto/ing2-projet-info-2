#include <iostream>
#include <fstream>
#include <vector>
#include "Pion.h"
#include <jouer.h>
#include <Damier.h>
#include <cstdlib>
#include <console.h>
#include <conio.h>

using namespace std;

int main()
{


    Console* pConsole = NULL;
    pConsole = Console::getInstance();

    int curX=14;
    int curY=3;

    int quit=0;
    while(quit==0)
    {

        char choix='0';

        while((choix!='s')&&(choix!='z')&&(choix!=' '))
        {
            system("cls");

            std::cout<<"                      OTHELLO"<<std::endl<<std::endl<<std::endl;
            std::cout<<"              1 - Nouvelle Partie"<<std::endl;
            std::cout<<"              2 - Charger Partie" <<std::endl;
            std::cout<<"              3 - Regles"<<std::endl;
            std::cout<<"              4 - Credit"<<std::endl;
            std::cout<<"              5 - Quitter"<<std::endl;

            pConsole->Locate(curX,curY);

            choix=getch();

        }

        switch(choix)
        {
        case 's':
        {
            if(curY<7)
            {
                curY=curY+1;

            }
            break;

        }
        case 'z':
        {
            if(curY>3)
            {
                curY=curY-1;
            }
            break;
        }
        case ' ':
        {
            if(curY==3)
            {
                int bcl=0;
                curX=3;
                curY=1;

                while(bcl==0)
                {


                    char choix2='0';

                    pConsole->Locate(curX,curY);
                    while((choix2!=' ')&&(choix2!='s')&&(choix2!='z'))
                    {

                        system("cls");

                        std::cout<<std::endl<<"   1 - Joueur VS joueur        "<<std::endl;
                        std::cout<<"   2 - Joueur VS IA debutant   " <<std::endl;
                        std::cout<<"   3 - Joueur VS IA intermediaire  "<<std::endl;
                        std::cout<<"   4 - Quitter  ";

                        pConsole->Locate(curX,curY);
                        choix2=getch();
                    }

                    switch(choix2)
                    {
                    case 's':
                    {
                        if(curY<3)
                        {
                            curY=curY+1;

                        }
                        break;

                    }
                    case 'z':
                    {
                        if(curY>1)
                        {
                            curY=curY-1;
                        }
                        break;
                    }
                    case ' ':
                    {
                        if(curY==1)
                        {
                            curX=9;
                            curY=2;
                            int esc=0;
                            while(esc==0)
                            {


                                char choix3='0';

                                pConsole->Locate(curX,curY);
                                while((choix3!='s')&&(choix3!='z')&&(choix3!=' '))
                                {
                                    system("cls");

                                    std::cout<<"         activer assistance ?"<<std::endl<<std::endl;
                                    std::cout<<"         1 - Oui      "<<std::endl;
                                    std::cout<<"         2 - Non      "<<std::endl;
                                    std::cout<<"         3 - retour";
                                    pConsole->Locate(curX,curY);
                                    choix3=getch();

                                }
                                switch(choix3)
                                {
                                case 's':
                                {
                                    if(curY<4)
                                    {
                                        curY=curY+1;

                                    }
                                    break;

                                }
                                case 'z':
                                {
                                    if(curY>2)
                                    {
                                        curY=curY-1;
                                    }
                                    break;
                                }
                                case ' ':
                                {
                                    if (curY==2)
                                    {
                                        jouer NewJeu;
                                        NewJeu.jouerJvsJavecaide();
                                        curX=3;
                                        curY=1;
                                    }
                                    if (curY==3)
                                    {
                                        jouer NewJeu;
                                        NewJeu.jouerJvsJ();
                                        curX=3;
                                        curY=1;
                                    }
                                    if (curY==4)
                                    {
                                        esc=1;
                                        curX=3;
                                        curY=1;
                                    }
                                    break;
                                }
                                }
                            }


                        }
                        else if(curY==2)
                        {
                            system("cls");
                            char player;

                            while((player!='B')&&(player!='N'))
                            {
                                system("cls");
                                std::cout<<"jouer blanc: B ou noir N"<<std::endl;
                                std::cin>>player;
                            }

                            curX=9;
                            curY=2;
                            int esc=0;
                            while(esc==0)
                            {


                                char choix3='0';

                                pConsole->Locate(curX,curY);
                                while((choix3!='s')&&(choix3!='z')&&(choix3!=' '))
                                {
                                    system("cls");

                                    std::cout<<"         activer assistance ?"<<std::endl<<std::endl;
                                    std::cout<<"         1 - Oui      "<<std::endl;
                                    std::cout<<"         2 - Non      "<<std::endl;
                                    std::cout<<"         3 - retour";
                                    pConsole->Locate(curX,curY);
                                    choix3=getch();

                                }
                                switch(choix3)
                                {
                                case 's':
                                {
                                    if(curY<4)
                                    {
                                        curY=curY+1;

                                    }
                                    break;

                                }
                                case 'z':
                                {
                                    if(curY>2)
                                    {
                                        curY=curY-1;
                                    }
                                    break;
                                }
                                case ' ':
                                {
                                    if (curY==2)
                                    {
                                        jouer NewJeu;
                                        NewJeu.jouerIAaleaavecaide(player);
                                        esc=1;
                                        curX=9;
                                        curY=2;

                                    }
                                    if (curY==3)
                                    {
                                        jouer NewJeu;
                                        NewJeu.jouerIAalea(player);
                                        esc=1;
                                        curX=9;
                                        curY=2;

                                    }
                                    if (curY==4)
                                    {
                                        esc=1;
                                        curX=3;
                                        curY=1;
                                        player='0';
                                    }
                                    break;
                                }
                                }
                            }

                        }
                        else if(curY==3)
                        {

                            system("cls");
                            char player;

                            while((player!='B')&&(player!='N'))
                            {
                                system("cls");
                                std::cout<<"jouer blanc: B ou noir N"<<std::endl;
                                std::cin>>player;
                            }

                            curX=9;
                            curY=2;
                            int esc=0;
                            while(esc==0)
                            {


                                char choix3='0';

                                pConsole->Locate(curX,curY);
                                while((choix3!='s')&&(choix3!='z')&&(choix3!=' '))
                                {
                                    system("cls");

                                    std::cout<<"         activer assistance ?"<<std::endl<<std::endl;
                                    std::cout<<"         1 - Oui      "<<std::endl;
                                    std::cout<<"         2 - Non      "<<std::endl;
                                    std::cout<<"         3 - retour";
                                    pConsole->Locate(curX,curY);
                                    choix3=getch();

                                }
                                switch(choix3)
                                {
                                case 's':
                                {
                                    if(curY<4)
                                    {
                                        curY=curY+1;

                                    }
                                    break;

                                }
                                case 'z':
                                {
                                    if(curY>2)
                                    {
                                        curY=curY-1;
                                    }
                                    break;
                                }
                                case ' ':
                                {
                                    if (curY==2)
                                    {
                                        jouer NewJeu;
                                        NewJeu.jouerIAniv1avecaide(player);
                                        esc=1;
                                        curX=9;
                                        curY=2;

                                    }
                                    if (curY==3)
                                    {
                                        jouer NewJeu;
                                        NewJeu.jouerIAniv1(player);
                                        esc=1;
                                        curX=9;
                                        curY=2;

                                    }
                                    if (curY==4)
                                    {
                                        esc=1;
                                        curX=3;
                                        curY=1;
                                        player='0';
                                    }
                                    break;
                                }
                                }
                            }

                        }


                        else if(curY==4)
                        {
                           curX=14;
                           curY=3;
                            bcl=1;



                        }
                        break;
                    }


                    }



                }
            }

            if(curY==4)
            {

            }
            if(curY==5)
            {

            }
            if(curY==6)
            {
                system("cls");
                pConsole->Locate(15,4);
                std::cout<<"JEU REALISE PAR :"<<std::endl;
                pConsole->Locate(15,6);
                std::cout<<"Quentin CHABENNET"<<std::endl;
                pConsole->Locate(15,7);
                std::cout<<"Hugo FRIDLANSKY"<<std::endl;
                pConsole->Locate(15,8);
                std::cout<<"Pierre LEGRIS";
                getch();

            }
            if(curY==7)
            {
                quit=1;
            }

            break;
        }

        default:
        {
            std::cout<<"error";
            break;
        }

        }
    }







    return 0;

}

