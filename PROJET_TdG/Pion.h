#ifndef PION_H
#define PION_H


class Pion
{
public:
    Pion();
    Pion(char _couleur, int _x, int _y);
    virtual ~Pion();
    void sauvegarde();
    void lecture();
    char Getcouleur()
    {
        return m_couleur;
    }
    void Setcouleur(char val)
    {
        m_couleur = val;
    }
    int Getx()
    {
        return m_x;
    }
    void Setx(int val)
    {
        m_x = val;
    }
    int Gety()
    {
        return m_y;
    }
    void Sety(int val)
    {
        m_y = val;
    }
    void AfficherPion();

protected:

private:
    char m_couleur;
    int m_x;
    int m_y;

};

#endif // PION_H
